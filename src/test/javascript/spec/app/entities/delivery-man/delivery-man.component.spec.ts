/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import DeliveryManComponent from '@/entities/delivery-man/delivery-man.vue';
import DeliveryManClass from '@/entities/delivery-man/delivery-man.component';
import DeliveryManService from '@/entities/delivery-man/delivery-man.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('DeliveryMan Management Component', () => {
    let wrapper: Wrapper<DeliveryManClass>;
    let comp: DeliveryManClass;
    let deliveryManServiceStub: SinonStubbedInstance<DeliveryManService>;

    beforeEach(() => {
      deliveryManServiceStub = sinon.createStubInstance<DeliveryManService>(DeliveryManService);
      deliveryManServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<DeliveryManClass>(DeliveryManComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          deliveryManService: () => deliveryManServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      deliveryManServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllDeliveryMans();
      await comp.$nextTick();

      // THEN
      expect(deliveryManServiceStub.retrieve.called).toBeTruthy();
      expect(comp.deliveryMen[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      deliveryManServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(deliveryManServiceStub.retrieve.callCount).toEqual(1);

      comp.removeDeliveryMan();
      await comp.$nextTick();

      // THEN
      expect(deliveryManServiceStub.delete.called).toBeTruthy();
      expect(deliveryManServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
