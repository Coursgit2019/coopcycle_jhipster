import { IShoppingCart } from '@/shared/model/shopping-cart.model';

export interface IProduct {
  id?: number;
  name?: string;
  price?: number;
  description?: string;
  shoppingCarts?: IShoppingCart[] | null;
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string,
    public price?: number,
    public description?: string,
    public shoppingCarts?: IShoppingCart[] | null
  ) {}
}
