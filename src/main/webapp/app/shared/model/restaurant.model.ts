import { IShoppingCart } from '@/shared/model/shopping-cart.model';

export interface IRestaurant {
  id?: number;
  address?: string;
  phone?: string | null;
  shoppingCarts?: IShoppingCart[] | null;
}

export class Restaurant implements IRestaurant {
  constructor(public id?: number, public address?: string, public phone?: string | null, public shoppingCarts?: IShoppingCart[] | null) {}
}
