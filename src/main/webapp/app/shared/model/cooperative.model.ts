import { IShoppingCart } from '@/shared/model/shopping-cart.model';

export interface ICooperative {
  id?: number;
  website?: string | null;
  name?: string;
  adress?: string;
  shoppingCarts?: IShoppingCart[] | null;
}

export class Cooperative implements ICooperative {
  constructor(
    public id?: number,
    public website?: string | null,
    public name?: string,
    public adress?: string,
    public shoppingCarts?: IShoppingCart[] | null
  ) {}
}
