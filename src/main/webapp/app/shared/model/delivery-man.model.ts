import { ICourse } from '@/shared/model/course.model';

export interface IDeliveryMan {
  id?: number;
  name?: string;
  surname?: string;
  phone?: string | null;
  courses?: ICourse[] | null;
}

export class DeliveryMan implements IDeliveryMan {
  constructor(
    public id?: number,
    public name?: string,
    public surname?: string,
    public phone?: string | null,
    public courses?: ICourse[] | null
  ) {}
}
