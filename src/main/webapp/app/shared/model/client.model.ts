import { IShoppingCart } from '@/shared/model/shopping-cart.model';

import { Language } from '@/shared/model/enumerations/language.model';
export interface IClient {
  id?: number;
  clientId?: number;
  name?: string;
  surname?: string;
  address?: string;
  email?: string;
  phone?: string | null;
  language?: Language;
  shoppingCarts?: IShoppingCart[] | null;
}

export class Client implements IClient {
  constructor(
    public id?: number,
    public clientId?: number,
    public name?: string,
    public surname?: string,
    public address?: string,
    public email?: string,
    public phone?: string | null,
    public language?: Language,
    public shoppingCarts?: IShoppingCart[] | null
  ) {}
}
