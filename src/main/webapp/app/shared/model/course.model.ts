import { IShoppingCart } from '@/shared/model/shopping-cart.model';
import { IDeliveryMan } from '@/shared/model/delivery-man.model';

export interface ICourse {
  id?: number;
  idCourse?: number;
  idDeliveryMan?: string;
  time?: string;
  adress?: string | null;
  shoppingCart?: IShoppingCart | null;
  deliveryMan?: IDeliveryMan | null;
}

export class Course implements ICourse {
  constructor(
    public id?: number,
    public idCourse?: number,
    public idDeliveryMan?: string,
    public time?: string,
    public adress?: string | null,
    public shoppingCart?: IShoppingCart | null,
    public deliveryMan?: IDeliveryMan | null
  ) {}
}
