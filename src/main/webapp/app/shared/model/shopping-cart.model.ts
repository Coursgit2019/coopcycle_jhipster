import { ICourse } from '@/shared/model/course.model';
import { IClient } from '@/shared/model/client.model';
import { IRestaurant } from '@/shared/model/restaurant.model';
import { ICooperative } from '@/shared/model/cooperative.model';
import { IProduct } from '@/shared/model/product.model';

import { Payement } from '@/shared/model/enumerations/payement.model';
export interface IShoppingCart {
  id?: number;
  orderId?: number;
  total?: number;
  clientId?: number;
  cooperativeId?: number;
  restaurantId?: number;
  payement?: Payement;
  course?: ICourse | null;
  client?: IClient | null;
  restaurant?: IRestaurant | null;
  cooperative?: ICooperative | null;
  products?: IProduct[] | null;
}

export class ShoppingCart implements IShoppingCart {
  constructor(
    public id?: number,
    public orderId?: number,
    public total?: number,
    public clientId?: number,
    public cooperativeId?: number,
    public restaurantId?: number,
    public payement?: Payement,
    public course?: ICourse | null,
    public client?: IClient | null,
    public restaurant?: IRestaurant | null,
    public cooperative?: ICooperative | null,
    public products?: IProduct[] | null
  ) {}
}
