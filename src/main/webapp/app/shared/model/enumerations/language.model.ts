export enum Language {
  FRENCH = 'FRENCH',

  ENGLISH = 'ENGLISH',

  GREEK = 'GREEK',

  ITALIAN = 'ITALIAN',

  GERMAN = 'GERMAN',
}
