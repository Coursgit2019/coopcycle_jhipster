export enum Payement {
  CB = 'CB',

  Mastercard = 'Mastercard',

  Visa = 'Visa',

  Paypal = 'Paypal',

  Apple_Pay = 'Apple_Pay',

  Google_Pay = 'Google_Pay',

  Meal_Cheque = 'Meal_Cheque',

  Bitcoin = 'Bitcoin',

  Izly = 'Izly',
}
