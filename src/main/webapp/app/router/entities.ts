import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore
const Entities = () => import('@/entities/entities.vue');

// prettier-ignore
const Client = () => import('@/entities/client/client.vue');
// prettier-ignore
const ClientUpdate = () => import('@/entities/client/client-update.vue');
// prettier-ignore
const ClientDetails = () => import('@/entities/client/client-details.vue');
// prettier-ignore
const Course = () => import('@/entities/course/course.vue');
// prettier-ignore
const CourseUpdate = () => import('@/entities/course/course-update.vue');
// prettier-ignore
const CourseDetails = () => import('@/entities/course/course-details.vue');
// prettier-ignore
const DeliveryMan = () => import('@/entities/delivery-man/delivery-man.vue');
// prettier-ignore
const DeliveryManUpdate = () => import('@/entities/delivery-man/delivery-man-update.vue');
// prettier-ignore
const DeliveryManDetails = () => import('@/entities/delivery-man/delivery-man-details.vue');
// prettier-ignore
const ShoppingCart = () => import('@/entities/shopping-cart/shopping-cart.vue');
// prettier-ignore
const ShoppingCartUpdate = () => import('@/entities/shopping-cart/shopping-cart-update.vue');
// prettier-ignore
const ShoppingCartDetails = () => import('@/entities/shopping-cart/shopping-cart-details.vue');
// prettier-ignore
const Restaurant = () => import('@/entities/restaurant/restaurant.vue');
// prettier-ignore
const RestaurantUpdate = () => import('@/entities/restaurant/restaurant-update.vue');
// prettier-ignore
const RestaurantDetails = () => import('@/entities/restaurant/restaurant-details.vue');
// prettier-ignore
const Cooperative = () => import('@/entities/cooperative/cooperative.vue');
// prettier-ignore
const CooperativeUpdate = () => import('@/entities/cooperative/cooperative-update.vue');
// prettier-ignore
const CooperativeDetails = () => import('@/entities/cooperative/cooperative-details.vue');
// prettier-ignore
const Product = () => import('@/entities/product/product.vue');
// prettier-ignore
const ProductUpdate = () => import('@/entities/product/product-update.vue');
// prettier-ignore
const ProductDetails = () => import('@/entities/product/product-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default {
  path: '/',
  component: Entities,
  children: [
    {
      path: 'client',
      name: 'Client',
      component: Client,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'client/new',
      name: 'ClientCreate',
      component: ClientUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'client/:clientId/edit',
      name: 'ClientEdit',
      component: ClientUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'client/:clientId/view',
      name: 'ClientView',
      component: ClientDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'course',
      name: 'Course',
      component: Course,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'course/new',
      name: 'CourseCreate',
      component: CourseUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'course/:courseId/edit',
      name: 'CourseEdit',
      component: CourseUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'course/:courseId/view',
      name: 'CourseView',
      component: CourseDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'delivery-man',
      name: 'DeliveryMan',
      component: DeliveryMan,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'delivery-man/new',
      name: 'DeliveryManCreate',
      component: DeliveryManUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'delivery-man/:deliveryManId/edit',
      name: 'DeliveryManEdit',
      component: DeliveryManUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'delivery-man/:deliveryManId/view',
      name: 'DeliveryManView',
      component: DeliveryManDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shopping-cart',
      name: 'ShoppingCart',
      component: ShoppingCart,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shopping-cart/new',
      name: 'ShoppingCartCreate',
      component: ShoppingCartUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shopping-cart/:shoppingCartId/edit',
      name: 'ShoppingCartEdit',
      component: ShoppingCartUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shopping-cart/:shoppingCartId/view',
      name: 'ShoppingCartView',
      component: ShoppingCartDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'restaurant',
      name: 'Restaurant',
      component: Restaurant,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'restaurant/new',
      name: 'RestaurantCreate',
      component: RestaurantUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'restaurant/:restaurantId/edit',
      name: 'RestaurantEdit',
      component: RestaurantUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'restaurant/:restaurantId/view',
      name: 'RestaurantView',
      component: RestaurantDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative',
      name: 'Cooperative',
      component: Cooperative,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative/new',
      name: 'CooperativeCreate',
      component: CooperativeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative/:cooperativeId/edit',
      name: 'CooperativeEdit',
      component: CooperativeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative/:cooperativeId/view',
      name: 'CooperativeView',
      component: CooperativeDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product',
      name: 'Product',
      component: Product,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product/new',
      name: 'ProductCreate',
      component: ProductUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product/:productId/edit',
      name: 'ProductEdit',
      component: ProductUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product/:productId/view',
      name: 'ProductView',
      component: ProductDetails,
      meta: { authorities: [Authority.USER] },
    },
    // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
  ],
};
