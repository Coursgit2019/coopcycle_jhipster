import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import ShoppingCartService from '@/entities/shopping-cart/shopping-cart.service';
import { IShoppingCart } from '@/shared/model/shopping-cart.model';

import DeliveryManService from '@/entities/delivery-man/delivery-man.service';
import { IDeliveryMan } from '@/shared/model/delivery-man.model';

import { ICourse, Course } from '@/shared/model/course.model';
import CourseService from './course.service';

const validations: any = {
  course: {
    idCourse: {
      required,
      numeric,
    },
    idDeliveryMan: {
      required,
    },
    time: {
      required,
    },
    adress: {},
  },
};

@Component({
  validations,
})
export default class CourseUpdate extends Vue {
  @Inject('courseService') private courseService: () => CourseService;
  @Inject('alertService') private alertService: () => AlertService;

  public course: ICourse = new Course();

  @Inject('shoppingCartService') private shoppingCartService: () => ShoppingCartService;

  public shoppingCarts: IShoppingCart[] = [];

  @Inject('deliveryManService') private deliveryManService: () => DeliveryManService;

  public deliveryMen: IDeliveryMan[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.courseId) {
        vm.retrieveCourse(to.params.courseId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.course.id) {
      this.courseService()
        .update(this.course)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.course.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.courseService()
        .create(this.course)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.course.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveCourse(courseId): void {
    this.courseService()
      .find(courseId)
      .then(res => {
        this.course = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.shoppingCartService()
      .retrieve()
      .then(res => {
        this.shoppingCarts = res.data;
      });
    this.deliveryManService()
      .retrieve()
      .then(res => {
        this.deliveryMen = res.data;
      });
  }
}
