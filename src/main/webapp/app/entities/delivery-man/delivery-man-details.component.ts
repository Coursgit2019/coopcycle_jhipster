import { Component, Vue, Inject } from 'vue-property-decorator';

import { IDeliveryMan } from '@/shared/model/delivery-man.model';
import DeliveryManService from './delivery-man.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class DeliveryManDetails extends Vue {
  @Inject('deliveryManService') private deliveryManService: () => DeliveryManService;
  @Inject('alertService') private alertService: () => AlertService;

  public deliveryMan: IDeliveryMan = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.deliveryManId) {
        vm.retrieveDeliveryMan(to.params.deliveryManId);
      }
    });
  }

  public retrieveDeliveryMan(deliveryManId) {
    this.deliveryManService()
      .find(deliveryManId)
      .then(res => {
        this.deliveryMan = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
