import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IDeliveryMan } from '@/shared/model/delivery-man.model';

import DeliveryManService from './delivery-man.service';
import AlertService from '@/shared/alert/alert.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class DeliveryMan extends Vue {
  @Inject('deliveryManService') private deliveryManService: () => DeliveryManService;
  @Inject('alertService') private alertService: () => AlertService;

  private removeId: number = null;

  public deliveryMen: IDeliveryMan[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllDeliveryMans();
  }

  public clear(): void {
    this.retrieveAllDeliveryMans();
  }

  public retrieveAllDeliveryMans(): void {
    this.isFetching = true;
    this.deliveryManService()
      .retrieve()
      .then(
        res => {
          this.deliveryMen = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
          this.alertService().showHttpError(this, err.response);
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IDeliveryMan): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeDeliveryMan(): void {
    this.deliveryManService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('blogApp.deliveryMan.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllDeliveryMans();
        this.closeDialog();
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
