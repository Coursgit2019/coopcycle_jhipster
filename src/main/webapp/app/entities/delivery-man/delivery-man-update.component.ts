import { Component, Vue, Inject } from 'vue-property-decorator';

import { required, minLength, maxLength } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import CourseService from '@/entities/course/course.service';
import { ICourse } from '@/shared/model/course.model';

import { IDeliveryMan, DeliveryMan } from '@/shared/model/delivery-man.model';
import DeliveryManService from './delivery-man.service';

const validations: any = {
  deliveryMan: {
    name: {
      required,
    },
    surname: {
      required,
    },
    phone: {
      minLength: minLength(10),
      maxLength: maxLength(10),
    },
  },
};

@Component({
  validations,
})
export default class DeliveryManUpdate extends Vue {
  @Inject('deliveryManService') private deliveryManService: () => DeliveryManService;
  @Inject('alertService') private alertService: () => AlertService;

  public deliveryMan: IDeliveryMan = new DeliveryMan();

  @Inject('courseService') private courseService: () => CourseService;

  public courses: ICourse[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.deliveryManId) {
        vm.retrieveDeliveryMan(to.params.deliveryManId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.deliveryMan.id) {
      this.deliveryManService()
        .update(this.deliveryMan)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.deliveryMan.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.deliveryManService()
        .create(this.deliveryMan)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.deliveryMan.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveDeliveryMan(deliveryManId): void {
    this.deliveryManService()
      .find(deliveryManId)
      .then(res => {
        this.deliveryMan = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.courseService()
      .retrieve()
      .then(res => {
        this.courses = res.data;
      });
  }
}
