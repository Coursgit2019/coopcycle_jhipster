import { Component, Provide, Vue } from 'vue-property-decorator';

import UserService from '@/entities/user/user.service';
import ClientService from './client/client.service';
import CourseService from './course/course.service';
import DeliveryManService from './delivery-man/delivery-man.service';
import ShoppingCartService from './shopping-cart/shopping-cart.service';
import RestaurantService from './restaurant/restaurant.service';
import CooperativeService from './cooperative/cooperative.service';
import ProductService from './product/product.service';
// jhipster-needle-add-entity-service-to-entities-component-import - JHipster will import entities services here

@Component
export default class Entities extends Vue {
  @Provide('userService') private userService = () => new UserService();
  @Provide('clientService') private clientService = () => new ClientService();
  @Provide('courseService') private courseService = () => new CourseService();
  @Provide('deliveryManService') private deliveryManService = () => new DeliveryManService();
  @Provide('shoppingCartService') private shoppingCartService = () => new ShoppingCartService();
  @Provide('restaurantService') private restaurantService = () => new RestaurantService();
  @Provide('cooperativeService') private cooperativeService = () => new CooperativeService();
  @Provide('productService') private productService = () => new ProductService();
  // jhipster-needle-add-entity-service-to-entities-component - JHipster will import entities services here
}
