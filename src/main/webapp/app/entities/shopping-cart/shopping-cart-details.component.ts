import { Component, Vue, Inject } from 'vue-property-decorator';

import { IShoppingCart } from '@/shared/model/shopping-cart.model';
import ShoppingCartService from './shopping-cart.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ShoppingCartDetails extends Vue {
  @Inject('shoppingCartService') private shoppingCartService: () => ShoppingCartService;
  @Inject('alertService') private alertService: () => AlertService;

  public shoppingCart: IShoppingCart = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.shoppingCartId) {
        vm.retrieveShoppingCart(to.params.shoppingCartId);
      }
    });
  }

  public retrieveShoppingCart(shoppingCartId) {
    this.shoppingCartService()
      .find(shoppingCartId)
      .then(res => {
        this.shoppingCart = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
