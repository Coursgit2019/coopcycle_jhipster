import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import CourseService from '@/entities/course/course.service';
import { ICourse } from '@/shared/model/course.model';

import ClientService from '@/entities/client/client.service';
import { IClient } from '@/shared/model/client.model';

import RestaurantService from '@/entities/restaurant/restaurant.service';
import { IRestaurant } from '@/shared/model/restaurant.model';

import CooperativeService from '@/entities/cooperative/cooperative.service';
import { ICooperative } from '@/shared/model/cooperative.model';

import ProductService from '@/entities/product/product.service';
import { IProduct } from '@/shared/model/product.model';

import { IShoppingCart, ShoppingCart } from '@/shared/model/shopping-cart.model';
import ShoppingCartService from './shopping-cart.service';
import { Payement } from '@/shared/model/enumerations/payement.model';

const validations: any = {
  shoppingCart: {
    orderId: {
      required,
      numeric,
    },
    total: {
      required,
      numeric,
    },
    clientId: {
      required,
      numeric,
    },
    cooperativeId: {
      required,
      numeric,
    },
    restaurantId: {
      required,
      numeric,
    },
    payement: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class ShoppingCartUpdate extends Vue {
  @Inject('shoppingCartService') private shoppingCartService: () => ShoppingCartService;
  @Inject('alertService') private alertService: () => AlertService;

  public shoppingCart: IShoppingCart = new ShoppingCart();

  @Inject('courseService') private courseService: () => CourseService;

  public courses: ICourse[] = [];

  @Inject('clientService') private clientService: () => ClientService;

  public clients: IClient[] = [];

  @Inject('restaurantService') private restaurantService: () => RestaurantService;

  public restaurants: IRestaurant[] = [];

  @Inject('cooperativeService') private cooperativeService: () => CooperativeService;

  public cooperatives: ICooperative[] = [];

  @Inject('productService') private productService: () => ProductService;

  public products: IProduct[] = [];
  public payementValues: string[] = Object.keys(Payement);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.shoppingCartId) {
        vm.retrieveShoppingCart(to.params.shoppingCartId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.shoppingCart.id) {
      this.shoppingCartService()
        .update(this.shoppingCart)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.shoppingCart.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.shoppingCartService()
        .create(this.shoppingCart)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.shoppingCart.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveShoppingCart(shoppingCartId): void {
    this.shoppingCartService()
      .find(shoppingCartId)
      .then(res => {
        this.shoppingCart = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.courseService()
      .retrieve()
      .then(res => {
        this.courses = res.data;
      });
    this.clientService()
      .retrieve()
      .then(res => {
        this.clients = res.data;
      });
    this.restaurantService()
      .retrieve()
      .then(res => {
        this.restaurants = res.data;
      });
    this.cooperativeService()
      .retrieve()
      .then(res => {
        this.cooperatives = res.data;
      });
    this.productService()
      .retrieve()
      .then(res => {
        this.products = res.data;
      });
  }
}
