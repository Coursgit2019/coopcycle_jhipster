package info4.gl.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Duration;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Course.
 */
@Entity
@Table(name = "course")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "id_course", nullable = false)
    private Long idCourse;

    @NotNull
    @Column(name = "id_delivery_man", nullable = false)
    private String idDeliveryMan;

    @NotNull
    @Column(name = "time", nullable = false)
    private Duration time;

    @Column(name = "adress")
    private String adress;

    @JsonIgnoreProperties(value = { "course", "client", "restaurant", "cooperative", "products" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private ShoppingCart shoppingCart;

    @ManyToOne
    @JsonIgnoreProperties(value = { "courses" }, allowSetters = true)
    private DeliveryMan deliveryMan;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Course id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCourse() {
        return this.idCourse;
    }

    public Course idCourse(Long idCourse) {
        this.setIdCourse(idCourse);
        return this;
    }

    public void setIdCourse(Long idCourse) {
        this.idCourse = idCourse;
    }

    public String getIdDeliveryMan() {
        return this.idDeliveryMan;
    }

    public Course idDeliveryMan(String idDeliveryMan) {
        this.setIdDeliveryMan(idDeliveryMan);
        return this;
    }

    public void setIdDeliveryMan(String idDeliveryMan) {
        this.idDeliveryMan = idDeliveryMan;
    }

    public Duration getTime() {
        return this.time;
    }

    public Course time(Duration time) {
        this.setTime(time);
        return this;
    }

    public void setTime(Duration time) {
        this.time = time;
    }

    public String getAdress() {
        return this.adress;
    }

    public Course adress(String adress) {
        this.setAdress(adress);
        return this;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public ShoppingCart getShoppingCart() {
        return this.shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public Course shoppingCart(ShoppingCart shoppingCart) {
        this.setShoppingCart(shoppingCart);
        return this;
    }

    public DeliveryMan getDeliveryMan() {
        return this.deliveryMan;
    }

    public void setDeliveryMan(DeliveryMan deliveryMan) {
        this.deliveryMan = deliveryMan;
    }

    public Course deliveryMan(DeliveryMan deliveryMan) {
        this.setDeliveryMan(deliveryMan);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Course)) {
            return false;
        }
        return id != null && id.equals(((Course) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Course{" +
            "id=" + getId() +
            ", idCourse=" + getIdCourse() +
            ", idDeliveryMan='" + getIdDeliveryMan() + "'" +
            ", time='" + getTime() + "'" +
            ", adress='" + getAdress() + "'" +
            "}";
    }
}
