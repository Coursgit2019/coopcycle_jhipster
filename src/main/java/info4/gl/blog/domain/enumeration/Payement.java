package info4.gl.blog.domain.enumeration;

/**
 * The Payement enumeration.
 */
public enum Payement {
    CB,
    Mastercard,
    Visa,
    Paypal,
    Apple_Pay,
    Google_Pay,
    Meal_Cheque,
    Bitcoin,
    Izly,
}
