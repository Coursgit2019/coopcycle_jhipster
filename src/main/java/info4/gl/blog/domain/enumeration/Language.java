package info4.gl.blog.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    FRENCH,
    ENGLISH,
    GREEK,
    ITALIAN,
    GERMAN,
}
