package info4.gl.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import info4.gl.blog.domain.enumeration.Payement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ShoppingCart.
 */
@Entity
@Table(name = "shopping_cart")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ShoppingCart implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "order_id", nullable = false)
    private Long orderId;

    @NotNull
    @Column(name = "total", nullable = false)
    private Long total;

    @NotNull
    @Column(name = "client_id", nullable = false, insertable=false, updatable=false)
    private Long clientId;

    @NotNull
    @Column(name = "cooperative_id", nullable = false, insertable=false, updatable=false)
    private Long cooperativeId;

    @NotNull
    @Column(name = "restaurant_id", nullable = false, insertable=false, updatable=false)
    private Long restaurantId;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "payement", nullable = false)
    private Payement payement;

    @JsonIgnoreProperties(value = { "shoppingCart", "deliveryMan" }, allowSetters = true)
    @OneToOne(mappedBy = "shoppingCart")
    private Course course;

    @ManyToOne
    @JsonIgnoreProperties(value = { "shoppingCarts" }, allowSetters = true)
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties(value = { "shoppingCarts" }, allowSetters = true)
    private Restaurant restaurant;

    @ManyToOne
    @JsonIgnoreProperties(value = { "shoppingCarts" }, allowSetters = true)
    private Cooperative cooperative;

    @ManyToMany(mappedBy = "shoppingCarts")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "shoppingCarts" }, allowSetters = true)
    private Set<Product> products = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ShoppingCart id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return this.orderId;
    }

    public ShoppingCart orderId(Long orderId) {
        this.setOrderId(orderId);
        return this;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getTotal() {
        return this.total;
    }

    public ShoppingCart total(Long total) {
        this.setTotal(total);
        return this;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getClientId() {
        return this.clientId;
    }

    public ShoppingCart clientId(Long clientId) {
        this.setClientId(clientId);
        return this;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getCooperativeId() {
        return this.cooperativeId;
    }

    public ShoppingCart cooperativeId(Long cooperativeId) {
        this.setCooperativeId(cooperativeId);
        return this;
    }

    public void setCooperativeId(Long cooperativeId) {
        this.cooperativeId = cooperativeId;
    }

    public Long getRestaurantId() {
        return this.restaurantId;
    }

    public ShoppingCart restaurantId(Long restaurantId) {
        this.setRestaurantId(restaurantId);
        return this;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Payement getPayement() {
        return this.payement;
    }

    public ShoppingCart payement(Payement payement) {
        this.setPayement(payement);
        return this;
    }

    public void setPayement(Payement payement) {
        this.payement = payement;
    }

    public Course getCourse() {
        return this.course;
    }

    public void setCourse(Course course) {
        if (this.course != null) {
            this.course.setShoppingCart(null);
        }
        if (course != null) {
            course.setShoppingCart(this);
        }
        this.course = course;
    }

    public ShoppingCart course(Course course) {
        this.setCourse(course);
        return this;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ShoppingCart client(Client client) {
        this.setClient(client);
        return this;
    }

    public Restaurant getRestaurant() {
        return this.restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public ShoppingCart restaurant(Restaurant restaurant) {
        this.setRestaurant(restaurant);
        return this;
    }

    public Cooperative getCooperative() {
        return this.cooperative;
    }

    public void setCooperative(Cooperative cooperative) {
        this.cooperative = cooperative;
    }

    public ShoppingCart cooperative(Cooperative cooperative) {
        this.setCooperative(cooperative);
        return this;
    }

    public Set<Product> getProducts() {
        return this.products;
    }

    public void setProducts(Set<Product> products) {
        if (this.products != null) {
            this.products.forEach(i -> i.removeShoppingCart(this));
        }
        if (products != null) {
            products.forEach(i -> i.addShoppingCart(this));
        }
        this.products = products;
    }

    public ShoppingCart products(Set<Product> products) {
        this.setProducts(products);
        return this;
    }

    public ShoppingCart addProduct(Product product) {
        this.products.add(product);
        product.getShoppingCarts().add(this);
        return this;
    }

    public ShoppingCart removeProduct(Product product) {
        this.products.remove(product);
        product.getShoppingCarts().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShoppingCart)) {
            return false;
        }
        return id != null && id.equals(((ShoppingCart) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ShoppingCart{" +
            "id=" + getId() +
            ", orderId=" + getOrderId() +
            ", total=" + getTotal() +
            ", clientId=" + getClientId() +
            ", cooperativeId=" + getCooperativeId() +
            ", restaurantId=" + getRestaurantId() +
            ", payement='" + getPayement() + "'" +
            "}";
    }
}
